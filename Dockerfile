#FROM node:14.15.5-slim as node
#FROM nginx:1.18-alpine as nginx

FROM node:14.15.5-slim as node

WORKDIR /usr/src/app

RUN yarn add global gatsby-cli

ENV PATH="/node_modules/.bin:${PATH}"

COPY package*.json .
COPY yarn.lock .

RUN yarn install

COPY . .

RUN yarn clean && yarn build

FROM nginx:1.18-alpine as server

EXPOSE 80

COPY --from=node /usr/src/app/public /usr/share/nginx/html