# Tienda Gatsby C.D.M.T
  
## Construir y ejecutar Imagen

A, Ejecutar el siguiente Script con el siguiente comando
`sh start.sh` este se probo tanto en Ubutu, comoen windows en Gitbash.

> El script hara lo siguiente
>
> A, Crear imagen: `docker build --tag gatsby:1 .`
>
> - Nota: Si es la primera vez que se crea la imagen tardara varios
>  minutos por la descarga de node_modules, y despues para implementar
>  cambios, esta tardara alrededor de 3min, dependiendo de maquina y
> conexion.
>
> B, Levantar contenedor: `docker run --name gatsby_PROD -p 80:80 gatsby:1`
