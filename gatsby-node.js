/** See: https://www.gatsbyjs.com/docs/node-apis/ */
const path = require('path');

//INFORMACION
const products_all = require(`./src/data/products.json`);
const posts_list = require(`./src/data/posts.json`);

//CONSTANTES
const PRODUCT_DETAILS_TEMPLATE = 'src/templates/detalles-producto.js';
const POST_DETAILS_TEMPLATE = 'src/templates/detalles-post.js';

exports.createPages = async ({ actions, graphql }) => {
  const { createPage } = actions;
  const allProductsResp = getAllProducts();
  generateProductDetailPages(allProductsResp, createPage)

  const allPosts = getAllPosts();
  generatePostDetailPages(allPosts, createPage);
}

const generateProductDetailPages = (produtList, createPage) => {
  console.log('Total: ', produtList.lenth);
  produtList.forEach(product => {
    let endpoint = getUrlFormat(product.name);
    createPage({
      path: `productos/${endpoint}`,
      component: path.resolve(PRODUCT_DETAILS_TEMPLATE),
      context: {
        data: product,
        slug: endpoint,
      }
    });
    console.log("Producto generando: " + endpoint);
  });
}

const getAllProducts = () => {
  return products_all;
}


const generatePostDetailPages = (postList, createPage) => {
  console.log('Total: ', postList.lenth);
  postList.forEach(post => {
    let endpoint = getUrlFormat(post.title);
    createPage({
      path: `publicaciones/${endpoint}`,
      component: path.resolve(POST_DETAILS_TEMPLATE),
      context: {
        data: post,
        slug: endpoint,
      }
    });
    console.log("Publicacion generanda: " + endpoint);
  });
}

const getAllPosts = () => {
  return posts_list;
}



function getUrlFormat(name) {
  let urlFormat = '';
  name = name.toLowerCase();
  urlFormat = name.replace(/\s/g,"-"); // sustituir espacios blancos
  return urlFormat;
}