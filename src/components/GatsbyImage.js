import React from "react"
import { StaticQuery, graphql } from "gatsby";
import Img from "gatsby-image"

export default function GatsbyImage(props) {
  const { imageName, alt } = props;
  const query = graphql`
    query allFluidImages {
      allFile {
        edges { node { name childImageSharp { fluid { ...GatsbyImageSharpFluid } } } } 
      }
    }
  `;

  const renderImage = (respQuery) => {
      let image = respQuery.allFile.edges.find(image => {
        return image.node.name === imageName;
      });
      //console.log('image finded', image);
      if(!image) { 
        image = respQuery.allFile.edges.find(image => {
          return image.node.name === 'not-found-image';
        });;
      }
// notar que usamos componente que recien instalamos
      return <Img alt={alt} fluid={image.node.childImageSharp.fluid} />
  }
  
// el StaticQuery nos permitira renderizar a partir de una peticion grafphQl
  return (renderImage) 
    ? ( <StaticQuery query={query} render={renderImage} /> )
    : ( null );
}