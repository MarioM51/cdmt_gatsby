import React from 'react'

import { cleanHtml } from "./../../../utils/StringUtils";
import "./AboutUs.css";

const AboutUs = ( {foundingDate, description} ) => {
  return (
    <div className="about-us-container">
      
      <div className="about-us-data">
        <div><span>Fundacion:</span> {foundingDate}</div>
      </div>
      <div className="about-us-description">
        <p dangerouslySetInnerHTML={{__html: cleanHtml(description)}}></p>
      </div>
    </div>
  )
}

export default AboutUs;
