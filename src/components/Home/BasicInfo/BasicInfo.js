import React from 'react'

import ContactInfo from "./../ContactInfo";
import AboutUs from "./../AboutUs";
import TitleDivider from "./../../TitleDivider";

export default function BasicInfo({basicInfo}) {
  
  return (
    <div>
      <TitleDivider title="Informacion de contacto" />
      <ContactInfo 
        telephone={basicInfo.telephone}
        social_networks={basicInfo.social_networks}
        address={basicInfo.address}
        email={basicInfo.email}
      />
      <TitleDivider title="Sobre Nosotros" />
      <AboutUs
        foundingDate={basicInfo.foundingDate}
        description={basicInfo.description}
      />
    </div>
  )
}
