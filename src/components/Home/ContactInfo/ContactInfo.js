import React from 'react'
import PropTypes from "prop-types"

//import SocialNetworks from "./../SocialNetworks";
import "./ContactInfo.css";

const ContactInfo = ({ telephone, social_networks, address, email  }) => {
  return (
    <div className="contact-info-container">
      <div className="contact-info">
        <div><span>Telefono:</span> {telephone}</div>
        <div><span>Ubicacion:</span> {address.streetAddress}</div>
        <div><span>Email:</span> {email}</div>
      </div>
    </div>
  )
}

ContactInfo.propTypes = {
  telephone: PropTypes.string.isRequired,
  social_networks: PropTypes.array,
  address: PropTypes.object.isRequired,
  email: PropTypes.string.isRequired,
}

export default ContactInfo;