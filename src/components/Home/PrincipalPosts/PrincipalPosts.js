import React from 'react'
import { Link } from 'gatsby';

import TitleDivider from './../../TitleDivider';
import Post from './../../Posts/Post';
import './PrincipalPosts.css';

export default function PrincipalPosts({posts}) {
  return (
    <div className="principal-posts-container">
      <TitleDivider title="Publicaciones" />
      <div className="principal-posts-items">
        {posts &&
          posts.map((p,i)=>(
            <Post key={i} post={p} />
          ))
        }
      </div>
      <div className="principal-posts-bottom">
        <Link to="/publicaciones">
          VER MAS
        </Link>
      </div>
      
    </div>
  )
}
