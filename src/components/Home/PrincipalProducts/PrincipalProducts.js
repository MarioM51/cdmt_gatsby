import React from 'react'
import { Link } from 'gatsby';

import TitleDivider from './../../TitleDivider';
import Product from './../../Products/Product';

import './PrincipalProducts.css';

export default function PrincipalProducts({products}) {
  return (
    <div className="principal-products-container">
      <TitleDivider title="Productos" />
      <div className="principal-products-items">
        {products &&
          products.map((p,i)=>(
            <Product key={i} product={p} />
          ))
        }
      </div>
      <div className="principal-products-bottom">
        <Link to="/productos">
          VER MAS
        </Link>
      </div>
    </div>
  )
}
