import React from 'react'
import { Link } from 'gatsby';

import "./Layout.css";
import ContactInfo from "./../Home/ContactInfo";
import basic_info from './../../data/basic_info.json';

export default function Layout({ children, isHome }) {
  return (
    <div className="layout-contaioner">
      <header>
        <h1><Link to={'/'}>{basic_info.name}</Link></h1>
        <nav>
          <Link to={isHome ? '#productos' : '/productos'} >Productos</Link>
          <Link to={isHome ? '#publicaciones' : '/publicaciones'} >Publicaciones</Link>
          <Link to={isHome ? '#contacto' : '/#contacto'} >Contacto</Link>
          <Link to={isHome ? '#contacto' : '/#contacto'} >Sobre Nosotros</Link>
        </nav>
      </header>
      {children}
      <footer>
        <div className="footer-sections">
          <ContactInfo 
            telephone={basic_info.telephone}
            social_networks={basic_info.social_networks}
            address={basic_info.address}
            email={basic_info.email}
          />
          <div className="divider-vertical" />
          <div className="info-extra">
              <div><Link to="#" >Sitemap</Link></div>
              <div>Desarrollado Por: <Link to="#">Mario</Link></div>
              <div><Link to="#">Terminos y Condiciones</Link></div>
              <div><Link to="#">Aviso de privacidad</Link></div>
          </div>
        </div>
      </footer>
    </div>
  )
}
