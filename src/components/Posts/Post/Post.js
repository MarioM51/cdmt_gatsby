import React from 'react'
import { Link } from 'gatsby';

import { getUrlFormat } from "./../../../utils/StringUtils";
import GatsbyImage from "./../../GatsbyImage";
import './Post.css';

export default function Post({post}) {
  let title = post.title;
  const slug = getUrlFormat(post.title);
  const url = '/publicaciones/' + slug;
  if(title.length >= 46 )  {
    title = title.substr(0, 49) + "...";
  }
  if(post.description.length >= 106 )  {
    post.description = post.description.substr(0, 109) + "...";
  }
  return (
    <article className="post-container">
      <Link to={url} >
        <div>
          <div className="image">
            <GatsbyImage imageName={slug} alt={post.title} />
          </div>
          <span className="title">{title}</span>
          <div className="description">
            {post.description}
          </div>
      </div>
    </Link>
  </article>
  )
}

