import React from 'react'

import GatsbyImage from "../../GatsbyImage";
import { cleanHtml, getUrlFormat } from "./../../../utils/StringUtils";
import './PostDetails.css';

export default function PostDetails( {post} ) {
  const slug = getUrlFormat(post.title);
  return (
    <article className="post-Details-container">
      <header>
        <h2>{post.title}</h2>
        <div>
          <GatsbyImage imageName={slug} alt={post.title} />
        </div>
      </header>
      <main dangerouslySetInnerHTML={{__html: cleanHtml(post.content)}}></main>
    </article>
  )
}
