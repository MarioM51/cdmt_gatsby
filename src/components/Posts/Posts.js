import React from 'react'

import Post from "./Post";
import './Posts.css';

export default function Posts({ posts }) {
  return (
    <div className="posts-container">
      {posts &&
        posts.map((p, i) => (
          <Post key={i} post={p} />
        ))
      }
    </div>
  )
}
