import React from 'react'
import { Link } from 'gatsby';

import { getUrlFormat } from "./../../../utils/StringUtils";
import GatsbyImage from "./../../GatsbyImage";
import './Product.css';

export default function Product({product}) {
  const slug = getUrlFormat(product.name);
  const url = '/productos/' + slug;
  if(product.name.length >= 46 )  {
    product.name = product.name.substr(0, 49) + "...";
  }
  if(product.price.length >= 106 )  {
    product.price = product.price.substr(0, 109) + "...";
  }
  return (
    <article className="product-container">
      <Link to={url} >
        <div>
          <div className="image">
            <GatsbyImage imageName={slug} alt={product.name} />
          </div>
          <span className="name">{product.name }</span>
          <div className="price">
            $ {product.price}
          </div>
      </div>
    </Link>
  </article>
  )
}

