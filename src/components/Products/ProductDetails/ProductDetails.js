import React from 'react'

import GatsbyImage from "./../../GatsbyImage";
import TitleDivider from "./../../TitleDivider";
import { getUrlFormat } from "./../../../utils/StringUtils";
import './ProductDetails.css';

export default function ProductDetails( {product, basicInfo} ) {
  return (
    <div className="product-details-container">
      <h2>{product.name}</h2>
      <div className="product">
        <div className="image">
          <ProductImage imageName={product.name} />
        </div>
        <div className="data" >
          <ProductData product={product} />
        </div>
      </div>
      <div className="pay-mode-container">
        <TitleDivider title="Modo de Pago" />
        <PayMode basicInfo={basicInfo} />
      </div>
    </div>
  )
}

function ProductImage({imageName}) {
  const slug = getUrlFormat(imageName);
  return (
    <div className="product-image" >
        <GatsbyImage imageName={slug} alt={imageName} />
    </div>
  )
}


function ProductData( {product} ) {
  return (
    <>
      <div className="price">Precio: {product.price}</div>
      <div className="extra-info">
        <div>
          <div>Disponibles:</div>
          <div>{product.InStock}</div>
        </div>
        <div>
          <div>Categoria:</div>
          <div>{product.category}</div>
        </div>
        <div className="description">
          <div>Description:</div>
          <div>{product.description}</div>
        </div>
      </div>
    </>
  )
}


function PayMode( {basicInfo} ) {

  return (  
    <div className="pay-mode">
      <div className="by-phone">
        <div className="name">A Distancia</div>
        <div className="info">
          <div>Telefono/Whatsapp:<br/>{basicInfo.telephone}</div>
          <div>Numero de Cuenta:<br/>{basicInfo.payment_account}</div>
        </div>
        <div className="steps">
          1. Llamanos y dinos que productos quieres.<br />
          2. Dinos a donde enviarte el pedido.<br />
          3, Realiza el deposito.<br />
          4, Te enviamos los productos.<br />
        </div>
      </div>
      <div className="divider-vertical" />
      <div className="in-person">
          <div className="name">
            Precencial
          </div>
          <div className="info">
            <div>Con gusto te atenderemos en nuestras instalaciones</div>
          </div>
          <div className="info-m">
            <div>Calle: {basicInfo.address.streetAddress + ", " + basicInfo.address.addressLocality}</div>
            <div><a href={basicInfo.map} target="_blank" rel="noopener noreferrer">Mapa</a></div>
          </div>
          <div className="steps">
            Puntos de referencia: {basicInfo.address.howToArrive}
          </div>
      </div>
    </div>
  )
}