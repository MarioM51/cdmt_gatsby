import React from 'react'
import Product from "./Product";

import './Products.css';

export default function Products({products}) {
  return (
    <div className="products-container">
      {products && products.map((p, i) => (
        <Product key={i} product={p} />
      ))}
    </div>
  )
}
