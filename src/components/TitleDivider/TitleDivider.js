import React from 'react'

import "./TitleDivider.css";

export default function TitleDivider({title}) {
  let titleId = title;
  if(title.includes('contacto')) {
    titleId = 'contacto';
  }
  return (
    <div className="title-divider">
      <h2 id={titleId.toLowerCase()}>{title}</h2>
      <div className="divider"></div>
    </div>
  )
}
