import React, { useEffect, useState } from "react"
//import { Link } from "gatsby"

import Layout from "./../components/Layout"
import SEO from "../components/seo"
import BasicInfo from "./../components/Home/BasicInfo"
import PrincipalProducts from "./../components/Home/PrincipalProducts"
import PrincipalPosts from "./../components/Home/PrincipalPosts"

import products_principals from "./../data/products.json"
import basic_info from './../data/basic_info.json';
import post_list from './../data/posts.json';


export default function IndexPage() {
  const mainProducts = products_principals.filter(p => { return p.principal === true } );
  const mainPosts = post_list.filter(post => { return post.principal === true } );

  const [starsCount, setStarsCount] = useState(0)
  useEffect(() => {
    // get data from GitHub api
    fetch(`https://api.github.com/repos/gatsbyjs/gatsby`)
      .then(response => response.json()) // parse JSON from request
      .then(resultData => {
        setStarsCount(resultData.stargazers_count)
      }) // set data for the number of stars
  }, [])
  
  return (
  <Layout isHome={true} >
    <SEO title="Home" />
    <p>Runtime Data: Star count for the Gatsby repo {starsCount}</p>
    <PrincipalProducts products={mainProducts} />
    <PrincipalPosts posts={mainPosts} />
    <BasicInfo basicInfo={basic_info} />
  </Layout>
  )
}

