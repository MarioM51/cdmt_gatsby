import React from 'react'
import Layout from '../components/Layout'
import Products from '../components/Products'
import SEO from '../components/seo'

import products_page_1 from './../data/products.json';

export default function Productos() {
  //products_page_1.forEach(s => console.log(getUrlFormat(s.name) ) );
  return (
    <Layout>
      <SEO title="Productos" />
      <Products products={products_page_1} />
    </Layout>
  )
}