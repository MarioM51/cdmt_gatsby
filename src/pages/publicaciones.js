import React from 'react'
import Layout from '../components/Layout'
import Posts from '../components/Posts'
import SEO from '../components/seo'

import post_list from './../data/posts.json';

export default function Publicaciones() {
  return (
    <Layout>
      <SEO title="Publicaciones" />
      <Posts posts={post_list} />
    </Layout>
  )
}
