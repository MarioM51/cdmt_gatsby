import React from 'react'

import Layout from '../components/Layout'
import SEO from '../components/seo'
import PostDetails from '../components/Posts/PostDetails'

export default function DetallesPost(props) {
  const post = props.pathContext.data;
  return (
    <Layout>
      <SEO title={post.title} />
      <PostDetails post={post}  />
    </Layout>
  )
}
