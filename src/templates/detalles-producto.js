import React from 'react'

import Layout from '../components/Layout'
import SEO from '../components/seo'
import ProductDetails from '../components/Products/ProductDetails'

//import products_1 from './../data/products_1';
import basic_info from './../data/basic_info.json';

export default function DetallesProducto(props) {
  const product = props.pathContext.data;
  return (
    <Layout>
      <SEO title={product.name} />
      <ProductDetails product={product} basicInfo={basic_info} />
    </Layout>
  )
}
