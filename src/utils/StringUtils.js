import sanitizeHtml from 'sanitize-html';

export function getUrlFormat(name) {
  let urlFormat = '';
  name = name.toLowerCase();
  urlFormat = name.replace(/\s/g,"-"); // sustituir espacios blancos
  return urlFormat;
}


export function cleanHtml(dirty) {
  let clean = sanitizeHtml(dirty, {
    allowedTags: ['b', 'strong', 'i', 'p', 'div', 'iframe', 'span', 'a', 'ul', 'li', 'h1','h2','h3','h4','h5', 'img'],
    allowedAttributes: {
      a: ['href', 'target'],
      img: ['src'],
      iframe: ['src', 'width', 'height', 'frameborder', 'allow', 'allowfullscreen' ]
    },
    allowedIframeHostnames: ['www.youtube.com']
  });
  return clean;
}