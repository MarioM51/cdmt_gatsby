#!/bin/bash

image_name='gatsby:1';
container_name='gatsby_1';

# si esta corriendo el contenedor detenlo
if [ "$( docker container inspect -f '{{.State.Status}}' $container_name )" == "running" ];
  then
    docker stop $container_name
    echo "#########: $container_name Detenido"
  else
    echo "#########: There is no container $container_name running to stop"
fi

# construille la nueva imagen
docker build --tag $image_name .
echo "#########: $container_name Builded"

# levanta la nueva imagen
docker run -d --rm -p 80:80 --name $container_name $image_name
echo "#########: $container_name instantiated"
